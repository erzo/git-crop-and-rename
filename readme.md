When I started using git I did not have an account on a git server yet.
Git required me to insert something as a user name and I did not have the time to really think it through so I just entered something arbitrary.
But when I reached the point where I wanted to publish my software something arbitrary was not good enough anymore.
I wanted to replace my user name in the entire git history with my account name without changing any of the contents.
What I did not know back then was that this is far from trivial.
It means rewriting history. Tags and notes must be recreated on new commits. And through all of that committer and author dates shall not change.
But I wrote this program to do just that.

Output of `./main.py --help`:

    usage: main.py [-h] [--newroot SHA1] [--names NAMES [NAMES ...]]
                   [--emails EMAILS [EMAILS ...]] [--keep-notes] [--prune]
                   [--tmpbranch NAME] [--loglevel debug|info|warning|error|fatal]
    
    This script crops the history and changes the user name and e-mail address.
    Afterwards it updates (long and short) commit hash ids in commit messages
    and notes and restores the tags and notes.
    
    *WARNING*: since writing this script I have learned that short hashes can
    vary in length. This script currently recognizes the default length of seven
    characters, only. The regular expression REO_HASH can easily be altered to
    match other lengthes as well but I am not entirely sure about the implications
    when replacing these hashes. I guess it would work in most cases but it might
    happen that the replaced short hashes are not unique.
    https://git-scm.com/book/en/v2/Git-Tools-Revision-Selection#_short_sha_1
    
    When runing this script an editor will open up to edit the (new) root commit
    message. If you are not using --newroot you can ignore this, just close the
    editor without saving. If you are using --newroot change the message to an
    appropriate message for your new root commit, save and close the editor.
    You can choose the editor as usual by setting the EDITOR or GIT_EDITOR envir-
    onment variables or with `git config core.editor <editorname>`.
    
    This script creates two new files called log_before.txt and log_after.txt.
    log_before.txt displays the git history before running this script.
    log_after.txt displays the git history after running this script.
    A diff of these two files is automatically displayed at the end of this
    program. If you are satisfied with the result you can remove these files.
    
    In contrast to git-rebasetags, this script identifies commits by their index
    instead of their name. Removing commits in between is therefore not possible.
    https://ownyourbits.com/2017/08/14/rebasing-in-git-without-losing-tags/
    
    Prerequisites:
      - This script assumes a straight forward history. One branch, only trivial
        merges. Tested on a history like the following:
    
            * Merge 2
            |\
            |* Commit 4
            |* Commit 3
            |/
            * Merge 1
            |\
            |* Commit 2
            |* Commit 1
            |/
            * Root
    
        This branch does *not* need to be called "master".
        I don't know what would happen in more complex repositories. (Comments or
        improvements are appreciated.)
    
      - This script rewrites history. Therefore it may *not* be used on public
        repositories. Use at your own risk.
    
      - If there are other people with access to this repository get everyone's
        explicit permission to rewrite history first.
    
      - This script temporarily changes the GIT_EDITOR to sed. Therefore it probably
        does not work on Windows. Running on Mac OS might require minor changes.
    
      - Commit or stash all your changes before running this program.
    
      - I recommend you make a copy of the directory in which the repository is
        located before running this program---just in case.
    
    optional arguments:
      -h, --help            show this help message and exit
      --newroot SHA1, -r SHA1
                            If this option is specified the history will be
                            cropped. It takes one argument, the hash of the
                            commit which shall (after cropping) become the
                            first commit, the commit in which all previously
                            preceding commits will be combined.
      --names NAMES [NAMES ...], -n NAMES [NAMES ...]
                            If this option is specified committer, author and
                            tagger names will be changed. It takes one or more
                            mappings "old name -> new name" as argument. "new
                            name" (ommitting old name and separator) specifies
                            a default name which will be used if no matching
                            old name is specified. If no default value is
                            specified other values will not be changed. If this
                            option is specified several times the values will
                            be appended.
      --emails EMAILS [EMAILS ...], -m EMAILS [EMAILS ...]
                            If this option is specified committer, author and
                            tagger e-mails will be changed. It takes one or
                            more mappings "old e-mail -> new e-mail" as
                            argument. "new e-mail" (ommitting old e-mail and
                            separator) specifies a default e-mail which will be
                            used if no matching old e-mail is specified. If no
                            default value is specified other values will not be
                            changed. If this option is specified several times
                            the values will be appended.
      --keep-notes          By default all references to old notes will be
                            removed (not only commits, but all refs/notes/*).
                            Notes referring to still existing commits will be
                            recreated. If you want to remove the notes and not
                            just their references use --prune. If you do *not*
                            want to delete the old notes, use this option.
      --prune, -p           This script performs a rebase. Afterwards the old
                            commits will not be visible in the log anymore but
                            they will still be there. If you want to remove
                            them use this option. This option clears the reflog
                            and executes the garbage collector. WARNING: Be
                            absolutely sure this script does exactly what you
                            want before using this option. If you use this
                            option the rebase can not be undone.
      --tmpbranch NAME      This script creates a temporary branch in order to
                            perform the rebase. It's name is irrelevant as long
                            as it is different from any existing branch. In the
                            unlikely case that you have called your branch
                            "tmp" you can use this option to change the name of
                            the temporary branch.
      --loglevel debug|info|warning|error|fatal, -l debug|info|warning|error|fatal
