#!/usr/bin/env python3

# Copyright © 2020 erzo <erzo@posteo.de>
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See the copying.txt file for more details.

r"""
This script crops the history and changes the user name and e-mail address.
Afterwards it updates (long and short) commit hash ids in commit messages
and notes and restores the tags and notes.

*WARNING*: since writing this script I have learned that short hashes can
vary in length. This script currently recognizes the default length of seven
characters, only. The regular expression REO_HASH can easily be altered to
match other lengthes as well but I am not entirely sure about the implications
when replacing these hashes. I guess it would work in most cases but it might
happen that the replaced short hashes are not unique.
https://git-scm.com/book/en/v2/Git-Tools-Revision-Selection#_short_sha_1

When runing this script an editor will open up to edit the (new) root commit
message. If you are not using --newroot you can ignore this, just close the
editor without saving. If you are using --newroot change the message to an
appropriate message for your new root commit, save and close the editor.
You can choose the editor as usual by setting the EDITOR or GIT_EDITOR envir-
onment variables or with `git config core.editor <editorname>`.

This script creates two new files called log_before.txt and log_after.txt.
log_before.txt displays the git history before running this script.
log_after.txt displays the git history after running this script.
A diff of these two files is automatically displayed at the end of this
program. If you are satisfied with the result you can remove these files.

In contrast to git-rebasetags, this script identifies commits by their index
instead of their name. Removing commits in between is therefore not possible.
https://ownyourbits.com/2017/08/14/rebasing-in-git-without-losing-tags/

Prerequisites:
  - This script assumes a straight forward history. One branch, only trivial
    merges. Tested on a history like the following:

        * Merge 2
        |\
        |* Commit 4
        |* Commit 3
        |/
        * Merge 1
        |\
        |* Commit 2
        |* Commit 1
        |/
        * Root

    This branch does *not* need to be called "master".
    I don't know what would happen in more complex repositories. (Comments or
    improvements are appreciated.)

  - This script rewrites history. Therefore it may *not* be used on public
    repositories. Use at your own risk.

  - If there are other people with access to this repository get everyone's
    explicit permission to rewrite history first.

  - This script temporarily changes the GIT_EDITOR to sed. Therefore it probably
    does not work on Windows. Running on Mac OS might require minor changes.

  - Commit or stash all your changes before running this program.

  - I recommend you make a copy of the directory in which the repository is
    located before running this program---just in case.
"""

import subprocess
import os
import re
import logging

try:
	basestring
except:
	basestring = str


def one(iterable):
	'''checks if an iterable has at least one object. intended for filter.'''
	for i in iterable:
		return True
	return False


class Container(object):

	"""
	This is an abstract class.
	All sub classes must implement the __slots__ attribute,
	a tuple of attribute names which an object of the class is suppossed to have.
	"""

	__slots__ = tuple()

	def __init__(self, **kw):
		# prevent AttributeError on print
		for a in self.__slots__:
			setattr(self, a, None)

		# set attributes to the given values or trigger AttributeError if invalid attribute is given
		for key, val in kw.items():
			setattr(self, key, val)

	def __repr__(self):
		name = type(self).__name__
		out = "%s(\n" % name
		for a in self.__slots__:
			out += "   %s = %r,\n" % (a, self.__getattribute__(a))
		out += ")"
		return out


class Tag(Container):

	"""
	not-annotated tags do not have a timestamp:
	https://stackoverflow.com/questions/13208734/get-the-time-and-date-of-git-tags#comment75604202_13208734
	"""

	__slots__ = (
		"name",
		"sha1",
		"commit",
		"annotation",
		"timestamp",
		"taggername",
		"taggeremail",
	)


	def is_annotated(self):
		return self.annotation != None

	def __str__(self,
		format_annotated = \
"""{self.sha1} {self.name}
---> {self.commit}
timestamp: {self.timestamp}
tagger: {self.taggername} <{self.taggeremail}>
{self.annotation}""",
		format_not_annotated = \
"""{self.sha1} {self.name}
---> {self.commit}
<not annotated>"""
	):
		if self.is_annotated():
			f = format_annotated
		else:
			f = format_not_annotated
		return f.format(self=self)


class Note(Container):

	__slots__ = (
		"ref",
		"sha1",
		"commit",
		"content",
		"authordate",
		"authorname",
		"authoremail",
		"committerdate",
		"committername",
		"committeremail",
	)

class Commit(Container):

	__slots__ = (
		"tree",
		"authordate",
		"authorname",
		"authoremail",
		"committerdate",
		"committername",
		"committeremail",
	)


class GitAdapter(object):

	ENCODING = "utf-8"

	COMMITTER = "COMMITTER"
	COMMITTER_DATE  = "GIT_%s_DATE"  % COMMITTER
	COMMITTER_NAME  = "GIT_%s_NAME"  % COMMITTER
	COMMITTER_EMAIL = "GIT_%s_EMAIL" % COMMITTER

	AUTHOR = "AUTHOR"
	AUTHOR_DATE     = "GIT_%s_DATE"  % AUTHOR
	AUTHOR_NAME     = "GIT_%s_NAME"  % AUTHOR
	AUTHOR_EMAIL    = "GIT_%s_EMAIL" % AUTHOR


	# ---------- init ----------

	def __init__(self, dryrun):
		self.dryrun = dryrun


	# ---------- getter ----------

	def get_all_commit_hashes(self):
		cmd = ['git', 'log', '--format=%H']
		return self.run_get(cmd).splitlines()
	
	def get_root_commits(self):
		#https://stackoverflow.com/a/1007545
		cmd = ['git', 'rev-list', '--max-parents=0', 'HEAD']
		return self.run_get(cmd).splitlines()

	def get_all_commit_committerdates(self):
		cmd = ['git', 'log', '--format=%ci']
		return self.run_get(cmd).splitlines()


	def get_short_hash(self, long_hash):
		cmd = ['git', 'log', '-1', '--pretty=format:%h', long_hash, '--']
		short_hash = self.run_get(cmd).rstrip()
		assert short_hash
		return short_hash


	def get_all_tags(self):
		tags = list()

		fmt = "Tag(name=%(refname:short), sha1=%(objectname), commit=%(object), timestamp=%(taggerdate:raw), taggername=%(taggername), taggeremail=%(taggeremail))"
		for ln in self.iter_tags(fmt):
			tag = eval(ln)
			if tag.timestamp != "":
				a, c = self.get_tag_annotation_and_commit(tag.sha1)
				tag.annotation = a
				assert tag.commit == c

				# I don't know why but it seems taggeremail is always returned in angular brackets
				# I am stripping them
				assert tag.taggeremail[0] == '<' and tag.taggeremail[-1] == '>'
				tag.taggeremail = tag.taggeremail[1:-1]
			else:
				# an unannotated tag has no timestamp
				assert tag.commit == ""
				tag.annotation = None
				tag.commit = tag.sha1

			tags.append(tag)

		return tags

	def iter_tags(self, fmt):
		cmd = ['git', 'for-each-ref', '--python', '--format', fmt, 'refs/tags/*']
		for ln in self.run_get(cmd).splitlines():
			yield ln

	def iter_note_refs(self, fmt):
		cmd_ref = ['git', 'for-each-ref' ,'--format=%s' % fmt, 'refs/notes/*']
		return self.run_get(cmd_ref).splitlines()


	def is_tag_annotated(self, sha1):
		cmd = ['git', 'cat-file', '-t', sha1]
		t = self.run_get(cmd).strip()
		return t == 'tag'

	def get_tag_annotation_and_commit(self, sha1):
		cmd = ['git', 'cat-file', '-p', sha1]
		content = self.run_get(cmd).split('\n', 5)
		commit = content[0].split()[1]
		annotation = content[-1].rstrip()
		return annotation, commit


	def get_current_branch(self):
		cmd = ['git', 'branch']
		branches = self.run_get(cmd).splitlines()
		PREFIX = "* "
		i = len(PREFIX)
		for b in branches:
			if b[:i] == PREFIX:
				return b[i:]

		assert False

	def get_all_notes(self):
		#WARNING: relies on assumption that no fields contain newlines!
		# if a note is edited but the content not changed the corresponding commit is created but it's tree is the same. Therefore, an edit with no changes is ignored even if committer or author are different.
		notes = list()

		fmt = """
tree: %T
authordate: %ad
authorname: %an
authoremail: %ae
committerdate: %cd
committername: %cn
committeremail: %ce
""".strip()
		fmt += "\n"

		def note_history_commit(noteinfo):
			out = Commit()

			for ln in noteinfo.splitlines():
				key, val = ln.split(": ", 1)
				setattr(out, key, val)

			return out

		def ls(tree):
			cmd = ['git', 'ls-tree', tree]
			out = self.run_get(cmd)
			for ln in out.splitlines():
				mode, otype, sha1, filename = ln.split()
				yield mode, otype, sha1, filename

		def get_notes(commit, refname):
			newer_notes = list()
			logging.debug("")
			logging.debug("committer: %s <%s> (%s)", commit.committername, commit.committeremail, commit.committerdate)
			logging.debug("tree: %s", commit.tree)
			for mode, otype, sha1, filename in ls(commit.tree):
				logging.debug("%s, %s", sha1, filename)
				assert otype == 'blob'
				note = Note()

				note.commit = filename
				note.sha1 = sha1
				note.ref = refname

				cmd = ['git', 'cat-file', '-p', sha1]
				note.content = self.run_get(cmd)

				newer_notes.append(note)

			# check that there are no two notes referring to the same commit
			assert len(newer_notes) == len({n.commit for n in newer_notes})

			return newer_notes


		def update_notes(notes, changed_notes, commit):
			assert len(changed_notes) <= 1
			for n in changed_notes:
				if one(filter(lambda m: m.commit == n.commit and m.ref == n.ref, notes)):
					logging.debug("! skipping editing of note %s because it is already set", n.commit)
					continue

				logging.debug("! editing note %s = %s", n.commit, commit.committername)

				n.authordate  = commit.authordate
				n.authorname  = commit.authorname
				n.authoremail = commit.authoremail

				n.committerdate  = commit.committerdate
				n.committername  = commit.committername
				n.committeremail = commit.committeremail

				notes.append(n)

		fmt_note_refs = '%(objectname) %(refname)'
		for ref in self.iter_note_refs(fmt=fmt_note_refs):
			ref, refname = ref.split(" ", 1)
			cmd_log = ['git', 'log', '--format=%s' % fmt, ref]
			iter_log = iter(note_history_commit(c) for c in self.run_get(cmd_log).rstrip().split("\n\n"))

			# current notes
			commit = next(iter_log)
			logging.debug("notes history '%s'", ref)
			logging.debug("hash, filename")
			newer_notes = get_notes(commit, refname)

			# iterate through history of notes to extract author and committer
			for older_commit in iter_log:
				older_notes = get_notes(older_commit, refname)

				n = len(newer_notes) - len(older_notes)
				assert n == 0 or n == 1

				if n > 0:
					logging.debug("%s notes were added by %s", n, commit.committername)

					older_note_hashes = {n.sha1 for n in older_notes}
					note_hashes = {n.sha1 for n in newer_notes}
					assert older_note_hashes <= note_hashes

					older_note_names = {n.commit for n in older_notes}
					note_names = {n.commit for n in newer_notes}
					assert older_note_names < note_names

					changed_notes = tuple(filter(lambda n: n.commit not in older_note_names, newer_notes))
				else:
					older_note_names = {n.commit for n in older_notes}
					note_names = {n.commit for n in newer_notes}
					assert older_note_names == note_names

					changed_notes = list()
					for n in newer_notes:
						n_old = tuple(filter(lambda n_old: n_old.commit == n.commit, older_notes))
						assert len(n_old) == 1
						n_old = n_old[0]
						if n.sha1 != n_old.sha1:
							changed_notes.append(n)
					logging.debug("%s notes were edited by %s", len(changed_notes), commit.committername)


				update_notes(notes, changed_notes, commit)

				commit = older_commit
				newer_notes = older_notes

			changed_notes = newer_notes
			logging.debug("")
			logging.debug("%s first notes were added by %s", len(changed_notes), commit.committername)
			update_notes(notes, changed_notes, commit)
			logging.debug("")

		return notes


	def log(self, skip=None, n=None, fmt=None, notes=None, rev=None):
		cmd = ['git', 'log' ,'--format='+fmt]
		if skip:
			cmd.append('--skip')
			cmd.append(str(skip))
		if n:
			cmd.append('-n')
			cmd.append(str(n))
		if fmt:
			cmd.append('--format=%s' % fmt)

		if notes:
			cmd.append('--notes=%s' % notes)

		if rev:
			cmd.append(rev)

		return self.run_get(cmd)


	def tags(self, fmt=None):
		cmd = ['git', 'for-each-ref' ,'--format='+fmt, 'refs/tags/*']
		return self.run_get(cmd)


	# ---------- setter ----------

	def checkout(self, branch, commit=None, orphan=False):
		cmd = ['git', 'checkout']
		if orphan:
			cmd.append('--orphan')
		cmd.append(branch)
		if commit:
			cmd.append(commit)

		self.run_set(cmd)

	def commit(self, reenter_message=None, reuse_message=None, message=None, amend=False, author=None, env=None):
		cmd = ['git', 'commit', '--allow-empty']
		if reenter_message:
			cmd.append('-c')
			cmd.append(reenter_message)
		if reuse_message:
			cmd.append('-C')
			cmd.append(reuse_message)
		if message:
			cmd.append('--message')
			cmd.append(message)
		if amend:
			cmd.append('--amend')
		if author:
			cmd.append('--author')
			cmd.append(author)

		self.run_set(cmd, environment_variables=env)


	def rebase(self, onto, upstream, branch=None, interactive=False):
		cmd = ['git', 'rebase', '--keep-empty', '--rebase-merges', '--onto', onto, upstream]
		if branch != None:
			cmd.append(branch)

		if interactive:
			cmd.append('--interactive')
			# replace 'pick' with 'edit'
			# insert new line 'break' after 'merge'
			# print to stdout all new edit lines
			# print to stdout all merge lines
			editor = 'sed -i -e "/^pick/{" -e "s//edit/" -e "w /dev/stdout" -e "}" ' \
			                '-e "/^merge/{" -e "a break" -e "w /dev/stdout" -e "}" '
			env = dict(GIT_EDITOR=editor)
			stdout = subprocess.PIPE
		else:
			env = None
			stdout = None

		p = self.run_set(cmd, environment_variables=env, stdout=stdout)

		if stdout:
			stdout = p.stdout.decode(GitAdapter.ENCODING)
			stdout = stdout.splitlines()
			logging.debug("vvvvvvvvvv rebase vvvvvvvvvv")
			logging.debug("\n".join(stdout))
			logging.debug("^^^^^^^^^^ rebase ^^^^^^^^^^")
			return stdout

	def rebase_continue(self):
		cmd = ['git', 'rebase', '--continue']
		self.run_set(cmd)

	def branch_delete(self, branch):
		cmd = ['git', 'branch', '--delete', branch]
		self.run_set(cmd)


	def filter_branch(self, env_filter):
		cmd = ['git', 'filter-branch', '--env-filter', env_filter]
		self.run_set(cmd)


	def tag_replace(self, name, commit, annotation=None, date=None, taggername=None, taggeremail=None):
		environment_variables = None
		cmd = ['git', 'tag', '-f']
		if annotation:
			cmd.append('-a')
			cmd.append('-m')
			cmd.append(annotation)
			environment_variables = dict()
			environment_variables[GIT.COMMITTER_DATE] = date
			environment_variables[GIT.COMMITTER_NAME] = taggername
			environment_variables[GIT.COMMITTER_EMAIL] = taggeremail
			assert date != None
			assert taggername != None
			assert taggeremail !=None
		cmd.append(name)
		cmd.append(commit)
		self.run_set(cmd, environment_variables=environment_variables)

	def note_add(self, commit, content,
		committerdate, committername, committeremail,
		authordate, authorname, authoremail,
		ref,
	):
		cmd = ['git', 'notes', '--ref', ref, 'add', commit, '--message', content]
		env = {
			GIT.COMMITTER_DATE  : committerdate,
			GIT.COMMITTER_NAME  : committername,
			GIT.COMMITTER_EMAIL : committeremail,
			GIT.AUTHOR_DATE  : authordate,
			GIT.AUTHOR_NAME  : authorname,
			GIT.AUTHOR_EMAIL : authoremail,
		}
		self.run_set(cmd, environment_variables=env)


	# ---------- remover ----------

	def delete_ref(self, ref):
		#https://stackoverflow.com/questions/11829078/how-to-delete-git-notes-commit#comment60237737_11829460
		cmd = ['git', 'update-ref', '-d', ref]
		self.run_set(cmd)

	def collect_garbage(self):
		#https://stackoverflow.com/a/11688066

		cmd = ['git', 'reflog', 'expire', '--expire=now', '--all']
		self.run_set(cmd)

		cmd = ['git', 'gc', '--prune=now']
		self.run_set(cmd)


	# ---------- non-git commands ----------

	def diff(self, fn1, fn2):
		cmd = ['diff',
			'--color=always',
			'--unified=20',
			fn1,
			fn2,
		]
		self._run(cmd, check=False)


	# ---------- internal ----------

	def _run(self, cmd, stdout=None, check=True, environment_variables=None):

		# https://stackoverflow.com/a/4453495
		if environment_variables != None:
			env = os.environ.copy()
			for key, val in environment_variables.items():
				env[key] = val
		else:
			env = None

		return subprocess.run(cmd, stdout=stdout, check=check, env=env)

	def run_get(self, cmd):
		return self._run(cmd, stdout=subprocess.PIPE).stdout.decode(self.ENCODING)

	def run_set(self, cmd, environment_variables=None, stdout=None):
		if environment_variables:
			logging.info(environment_variables)
		logging.info(cmd)

		if self.dryrun:
			return 0

		return self._run(cmd, environment_variables=environment_variables, stdout=stdout)

GIT = GitAdapter



class GitCleaner(object):


	# ---------- init ----------

	def __init__(self, adapter, newroot, delete_old_notes, collect_garbage, tmpbranch):
		self.adapter = adapter
		self.newroot = newroot
		self.tmpbranch = tmpbranch
		self.fn_log_before = "log_before.txt"
		self.fn_log_after  = "log_after.txt"

		self.delete_old_notes = delete_old_notes
		self.collect_garbage = collect_garbage

		self.map_names = None
		self.map_emails = None

		self.new_commits = list()
		self.new_commits_short_hashes = list()

	def set_author_map(self, names, emails):
		self.map_names = names
		self.map_emails = emails


	# ---------- save ----------

	def save_golden_master(self, fn):
		fmt_commits = "fuller"
		fmt_tags = """
tag %(refname:short)%(if)%(taggername)%(then)
Tagger: %(taggername) %(taggeremail)
TaggerDate: %(taggerdate:iso)
Content:
%(contents)%(else)
<unannotated>
%(end)
""".strip('\n')

		with open(fn, 'wt') as f:
			f.write("(%s)\n\n" % fn)

			f.write("Tags\n")
			f.write("====\n")
			f.write(self.adapter.tags(fmt=fmt_tags))

			f.write("Commits\n")
			f.write("=======\n")
			f.write(self.adapter.log(fmt=fmt_commits, notes="*"))


	def save_old_commits(self):
		self.old_commits = self.adapter.get_all_commit_hashes()

		try:
			i = self.old_commits.index(self.newroot)
		except:
			commits = "\n".join("- %s" % c for c in self.old_commits)
			raise ValueError("this repo has the following commits:\n{commits}\nnewroot {newroot!r} is not one of them".format(newroot=self.newroot, commits=commits))

		i += 1
		logging.info("removing %s commits, keeping %s commits", len(self.old_commits)-i, i)
		self.old_commits = self.old_commits[:i]
		self.old_commits_short_hashes = [self.adapter.get_short_hash(long_hash) for long_hash in self.old_commits]


	def save_tags(self):
		self.oldtags = self.adapter.get_all_tags()

	def print_tags(self):
		for tag in self.oldtags:
			print(tag)
			print()

	def print_tags_repr(self):
		for tag in self.oldtags:
			print(repr(tag))
			print()

	def save_notes(self):
		self.oldnotes = self.adapter.get_all_notes()

	def print_notes(self):
		for note in self.oldnotes:
			print(note)
			print()

	def print_notes_repr(self):
		for note in self.oldnotes:
			print(repr(note))
			print()


	# ---------- cleanup ----------

	def cleanup(self):
		branch = self.adapter.get_current_branch()

		git = self.adapter
		git.checkout(orphan=True, branch=self.tmpbranch, commit=self.newroot)
		git.commit(reenter_message=self.newroot)

		i = len(self.old_commits)-1
		self.amend_commit(self.old_commits[i], update_message=False)

		changes = git.rebase(interactive=True, onto=self.tmpbranch, upstream=self.newroot, branch=branch)

		# -1 because root commit is not rebased. it is handled separately.
		assert len(changes) == len(self.old_commits) - 1

		for i in range(i-1,-1,-1):
			self.amend_commit(self.old_commits[i])
			git.rebase_continue()

		git.branch_delete(self.tmpbranch)

	def amend_commit(self, original_commit, update_message=True):
		env = self.get_env(original_commit)
		for name in (GIT.COMMITTER_NAME, GIT.AUTHOR_NAME):
			env[name] = self._get_new_value(env[name], self.map_names)
		for email in (GIT.COMMITTER_EMAIL, GIT.AUTHOR_EMAIL):
			env[email] = self._get_new_value(env[email], self.map_emails)

		an = env[GIT.AUTHOR_NAME]
		ae = env[GIT.AUTHOR_EMAIL]
		author = "%s <%s>" % (an, ae)

		if update_message:
			message = self.get_commit_message(original_commit)
			message = self.update_references(message)
			self.adapter.commit(amend=True, message=message, author=author, env=env)
		else:
			env['GIT_EDITOR'] = "echo >/dev/null"
			self.adapter.commit(amend=True, author=author, env=env)

		thiscommit = self.get_current_commit_hash()
		self.new_commits.insert(0, thiscommit)
		self.new_commits_short_hashes.insert(0, self.adapter.get_short_hash(thiscommit))

	REO_HASH = re.compile(r"\b([0-9A-F]{40}|[0-9A-F]{7})\b", re.IGNORECASE)
	def update_references(self, message):
		message = self.REO_HASH.sub(self._replace_ref, message)
		return message

	def _replace_ref(self, m):
		# I need to address the commits backwards here because new_commits is not complete yet
		old = m.group(0)
		if old in self.old_commits:
			i = self.old_commits.index(old)
			i = i - len(self.old_commits)
			new = self.new_commits[i]
		elif old in self.old_commits_short_hashes:
			i = self.old_commits_short_hashes.index(old)
			i = i - len(self.old_commits_short_hashes)
			new = self.new_commits_short_hashes[i]
		else:
			# this warning is not perfect because it does not include the new sha1 of the commit where it occurs. The new hash is not yet known because it will change after the amend. Postponing the warning would complicate the program unjustifiably. One can search for the hash in gitk or with git log | grep.
			logging.warning("Failed to update reference to the old commit %s", old)
			new = old

		return new

	def get_current_commit_hash(self):
		return self.adapter.log(fmt="%H", n=1).strip()

	def get_commit_message(self, commit):
		return self.adapter.log(fmt="%B", n=1, rev=commit)

	def get_env(self, commit):
		# I would like to use `git for-each-ref --python --count=1 --format=...`
		# but I don't see a possibility to get the data of the current commit with that.
		# Unfortunately, `git log` does not have a --python flag.
		# Instead I am relying on the assumption that no name, email or date contains a linebreak:
		git = self.adapter
		fmt = """
Committer Date  : %ci
Committer Name  : %cn
Committer E-Mail: %ce
Author Date  : %ai
Author Name  : %an
Author E-Mail: %ae
""".strip("\n")
		gitout = git.log(n=1, fmt=fmt, rev=commit)

		pattern = """
Committer Date  : (?P<cd>.*)
Committer Name  : (?P<cn>.*)
Committer E-Mail: (?P<ce>.*)
Author Date  : (?P<ad>.*)
Author Name  : (?P<an>.*)
Author E-Mail: (?P<ae>.*)
""".strip("\n")
		m = re.match(pattern, gitout)
		assert m

		env = {
			GIT.COMMITTER_DATE  : m.group('cd'),
			GIT.COMMITTER_NAME  : m.group('cn'),
			GIT.COMMITTER_EMAIL : m.group('ce'),
			GIT.AUTHOR_DATE  : m.group('ad'),
			GIT.AUTHOR_NAME  : m.group('an'),
			GIT.AUTHOR_EMAIL : m.group('ae'),
		}
		return env

	def _get_new_value(self, oldval, dict_old_new):
		if dict_old_new == None:
			return oldval

		if isinstance(dict_old_new, basestring):
			newval = dict_old_new
			return newval

		if oldval in dict_old_new:
			return dict_old_new[oldval]

		if None in dict_old_new:
			return dict_old_new[None]

		return oldval


	def delete_all_notes(self):
		fmt = "%(refname)"
		for ref in self.adapter.iter_note_refs(fmt=fmt):
			self.adapter.delete_ref(ref)


	# ---------- restore ----------

	#https://stackoverflow.com/a/29019547

	def restore_tags_with_new_tagger(self):
		for tag in self.oldtags:
			try:
				commit = self.new_commits[self.old_commits.index(tag.commit)]
			except ValueError:
				logging.error("Failed to find corresponding commit to tag {tag.name} which referred to the old commit {tag.commit}".format(tag=tag))
				continue

			if tag.is_annotated():
				name = self._get_new_value(tag.taggername, self.map_names)
				email = self._get_new_value(tag.taggeremail, self.map_emails)
				self.adapter.tag_replace(tag.name, commit,
					annotation = tag.annotation,
					date = tag.timestamp,
					taggername = name,
					taggeremail = email,
				)
			else:
				self.adapter.tag_replace(tag.name, commit)


	def restore_notes_with_new_committer(self):
		#reversed because log starts with newest, so I need to start with the last to get the same order
		for note in reversed(self.oldnotes):
			try:
				commit = self.new_commits[self.old_commits.index(note.commit)]
			except ValueError:
				logging.error("Failed to find corresponding commit to note {note.sha1} which referred to the old commit {note.commit}".format(note=note))
				continue

			content = note.content
			content = self.update_references(content)

			self.adapter.note_add(commit,
				ref = note.ref,
				content = content,
				committerdate = note.committerdate,
				committername = self._get_new_value(note.committername, self.map_names),
				committeremail = self._get_new_value(note.committeremail, self.map_emails),
				authordate = note.authordate,
				authorname = self._get_new_value(note.authorname, self.map_names),
				authoremail = self._get_new_value(note.authoremail, self.map_emails),
			)


	# ---------- main ----------

	def main(self):
		self.save_golden_master(self.fn_log_before)

		self.save_old_commits()
		self.save_tags()
		self.save_notes()
		#self.print_tags_repr()

		if self.delete_old_notes:
			self.delete_all_notes()

		self.cleanup()

		if self.collect_garbage:
			self.adapter.collect_garbage()

		self.restore_notes_with_new_committer()
		self.restore_tags_with_new_tagger()

		self.save_golden_master(self.fn_log_after)
		self.adapter.diff(self.fn_log_before, self.fn_log_after)


if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)

	class ParseMapping(argparse.Action):

		def __init__(self, option_strings, dest, **kwargs):
			kwargs.setdefault('default', dict())
			argparse.Action.__init__(self, option_strings, dest, **kwargs)

		def __call__(self, parser, namespace, values, option_string=None):
			out = getattr(namespace, self.dest)

			SEP = "->"
			for a in values:
				if SEP in a:
					old, new = a.split(SEP, 1)
					old = old.strip()
					new = new.strip()
				else:
					old = None
					new = a

				if old in out:
					raise argparse.ArgumentError(self, "key %s has been specified more than once." % old)

				out[old] = new

	names_help = """
If this option is specified committer, author and tagger names will be changed.
It takes one or more mappings "old name -> new name" as argument.
"new name" (ommitting old name and separator) specifies a default name which will be used if no matching old name is specified.
If no default value is specified other values will not be changed.
If this option is specified several times the values will be appended.
""".strip("\n")
	emails_help = names_help.replace("name", "e-mail")

	parser.add_argument('--newroot', '-r', nargs=1, metavar="SHA1",
		help = """
If this option is specified the history will be cropped.
It takes one argument, the hash of the commit which shall (after cropping) become the first commit,
the commit in which all previously preceding commits will be combined.
""".strip("\n"))
	parser.add_argument('--names', '-n',  nargs='+', action=ParseMapping, help=names_help)
	parser.add_argument('--emails', '-m', nargs='+', action=ParseMapping, help=emails_help)

	parser.add_argument('--keep-notes',  action='store_true',
		help = """
By default all references to old notes will be removed (not only commits, but all refs/notes/*).
Notes referring to still existing commits will be recreated.
If you want to remove the notes and not just their references use --prune.
If you do *not* want to delete the old notes, use this option.
""".strip("\n")
	)
	parser.add_argument('--prune', '-p', action='store_true',
		help = """
This script performs a rebase.
Afterwards the old commits will not be visible in the log anymore but they will still be there.
If you want to remove them use this option.
This option clears the reflog and executes the garbage collector.
WARNING: Be absolutely sure this script does exactly what you want before using this option.
If you use this option the rebase can not be undone.
""".strip("\n")
	)

	parser.add_argument('--tmpbranch', metavar='NAME', default="tmp",
		help = """
This script creates a temporary branch in order to perform the rebase.
It's name is irrelevant as long as it is different from any existing branch.
In the unlikely case that you have called your branch "tmp" you can use this
option to change the name of the temporary branch.
""".strip("\n")
	)

	loglevels = "debug|info|warning|error|fatal"
	parser.add_argument('--loglevel', '-l', metavar=loglevels, default=str(logging.DEBUG))

	args = parser.parse_args()

	loglevel = args.loglevel
	if loglevel.isnumeric():
		loglevel = int(loglevel)
	else:
		loglevel = loglevel.upper()
	logging.basicConfig(level=loglevel)

	a = GitAdapter(dryrun=False)
	roots = a.get_root_commits()
	assert len(roots) >= 1
	if len(roots) > 1:
		logging.error("There are several root commits.")
		logging.error("This program is intended for easier use cases.")
		logging.error("Please read the help and reconsider whether this is really the right tool for you.")
		exit(1)
	
	if args.newroot != None:
		newroot = args.newroot
	else:
		newroot = roots[0]
		logging.info("newroot is not specified. I am using %s." % newroot)

	delete_old_notes = not args.keep_notes
	collect_garbage = args.prune
	tmpbranch = args.tmpbranch

	c = GitCleaner(a, newroot, delete_old_notes=delete_old_notes, collect_garbage=collect_garbage, tmpbranch=tmpbranch)
	c.set_author_map(
		emails = args.emails,
		names = args.names,
	)
	#c.save_notes()
	#c.print_notes()
	c.main()
